angular.module('alurapic',['minhasDiretivas','ngAnimate', 'ngRoute', 'meuServicos'])
.config(function($routeProvider, $locationProvider){
	
	$locationProvider.html5Mode(true);
	
	$routeProvider.when('/fotos', {
		templateUrl: 'partials/principal.html',
		controller: 'FotosController' 
	});
	
	
	$routeProvider.when('/fotos/new', {
		templateUrl: 'partials/foto.html',
		controller: 'FotocadastroController'
		
	});
	
	
	$routeProvider.when('/fotos/edit/:fotoId', {
		templateUrl: 'partials/foto.html',
		controller: 'FotocadastroController'
		
	});
	
	$routeProvider.otherwise({
		redirectTo: '/fotos'
	});
	
});

