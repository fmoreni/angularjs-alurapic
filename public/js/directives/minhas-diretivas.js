angular.module('minhasDiretivas', [])
.directive('meuPainel', function(){
	var ddo = {};
	
	ddo.restric = "AE";
	
	ddo.scope = {
		titulo: '@'
	};
	
	ddo.transclude = true;
	
	ddo.templateUrl = 'js/directives/meu-painel.html';
	
	
	return ddo;
})
.directive('minhaFoto', function(){
	var ddo = {};
	
	ddo.restric = "AE";
	
	ddo.scope = {
		titulo: '@',
		url: '@'
	};
	

	ddo.template = '<img class="img-responsive center-block" src="{{url}}" alt="{{titulo}}">';
	
	return ddo;
})
.directive('meuBotaoPerigo', function(){
	var ddo = {};
	
	ddo.restric = "E";
	
	ddo.scope = {
		nome: '@',
		acao: '&' /* removendo acao do controle, avalia a expressao no contexto do controller */
	};
	
	ddo.template = '<button ng-click="acao(foto)" class="btn btn-danger btn-block">{{nome}}</button>'
	
	return ddo;
})
.directive('meuFoco', function(){
	var ddo = {};
	
	ddo.restric = "A";
	
	//<a href="/" class="btn btn-primary" meu-foco focado="focado">Voltar</a>
	ddo.link = function(scope, element){
		scope.$on('fotoCadastrada',function(){
			element[0].focus();
		});
	}
	
	
	return ddo;
	
})
.directive('meusTitulos', function(){

	var ddo = {};
	ddo.restrict = 'E';
	ddo.template = '<ul><li ng-repeat="titulo in titulos">{{titulo}}</li></ul>';
	ddo.controller = function($scope, recursoFoto) {
		recursoFoto.query(function(fotos) {
			$scope.titulos = fotos.map(function(foto) {
				return foto.titulo;
			});    
		});
	};
	
	return ddo;
	
})
;